#include<iostream>
#include<cstdlib>
#include<ctime>

using namespace std;

void display(int grid[10][10]);
void rider(int grid[10][10]);
void riderMovement(int grid[10][10]);

int main()
{
	cout<<"############################################"<<endl;
	cout << "#Welcome to 'Frodo & The Lord of the Rings'#"<<endl;
	cout<<"############################################"<<endl;
	
	int grid[10][10] = {};
	grid[0][0] = 1; // frodo starting point
    grid[9][9] = 3;	// exist space
    
    int f ; // store frodo movement input
	int status = 0 ; /* 0 = game continue
						1 = gameover */
	int level = 1 ;
	cout<<"\t\tLevel "<<level<<endl;
	
	rider(grid);	
	display(grid);
	
	int i=0 , j=0;
	do{
	
		cout << "[1-9] : ";
		cin >> f ;	
	
			switch(f)
			{	
				case 1 :
					if(i+1>9 || j-1 < 0){cout<<"\n\tInvalid. Please try again";} // edge checking
					else if(grid[i+1][j-1]==2) // dark rider captured frodo
					{
						cout << "\t\tGameover!" << endl;
						status = 1 ;
					}							
					else
					{ // valid movement
						grid[i+1][j-1]=1;
						grid[i][j]=0;
						// update frodo position
						i++;
						j--;
					}	
					break;
				case 2 :
					if(i+1>9){cout<<"\n\tInvalid. Please try again";}
					else if(grid[i+1][j]==2)
					{
						cout << "\t\tGameover!" << endl;
						status = 1 ;
					}
					else
					{
						grid[i+1][j]=1;
						grid[i][j]=0;
						i++;
					}	
					break;
				case 3 :
					if(i+1>9 || j+1 > 9){cout<<"\n\tInvalid. Please try again";}
					else if(grid[i+1][j+1]==2)
					{
						cout << "\t\tGameover!" << endl;
						status = 1 ;
					}
					else
					{
						grid[i+1][j+1]=1;
						grid[i][j]=0;
						i++;
						j++;
					}
					break;
				case 4 :
					if(j-1 < 0){cout<<"\n\tInvalid. Please try again";}
					else if(grid[i][j-1]==2)
					{
						cout << "\t\tGameover!" << endl;
						status = 1 ;
					}
					else
					{
						grid[i][j-1]=1;
						grid[i][j]=0;
						j--;
					}
					break;
				case 6 :
					if(j+1>9){cout<<"\n\tInvalid. Please try again";}
					else if(grid[i][j+1]==2)
					{
						cout << "\t\tGameover!" << endl;
						status = 1 ;
					}
					else
					{
						grid[i][j+1]=1;
						grid[i][j]=0;
						j++;
					}							
					break;
				case 7 :
					if(i-1<0 || j-1 < 0){cout<<"\n\tInvalid. Please try again";}
					else if(grid[i-1][j-1]==2)
					{
						cout << "\t\tGameover!" << endl;
						status = 1 ;
					}
					else
					{
						grid[i-1][j-1]==1;
						grid[i][j]=0;
						i--;
						j--;
					}						
					break;
				case 8 :
					if(i-1 < 0){cout<<"\n\tInvalid. Please try again";}
					else if(grid[i-1][j]==2)
					{
						cout << "\t\tGameover!" << endl;
						status = 1 ;
					}
					else
					{
						grid[i-1][j]=1;
						grid[i][j]=0;
						i--;
					}							
					break;
				case 9 :
					if(j+1>9 || i-1 < 0){cout<<"\n\tInvalid. Please try again";}
					else if(grid[i-1][j+1]==2)
					{
						cout << "\t\tGameover!" << endl;
						status = 1 ;
					}
					else
					{
						grid[i-1][j+1]=1;
						grid[i][j]=0;
						i--;
						j++;
					}						
					break;	
				default :
					cout << "\n\tInvalid. Please try again"<< endl;
					break;
			}
			
		riderMovement(grid);	// rider move after frodo moved				
			
		display(grid);	
			
		if(grid[9][9]==1) // check for win
			{
				level++; // level up
				cout<<"\t\tLevel Up!"<<endl;
				cout<<"\t\tLevel "<<level<<endl;
				// set F and R as game starting 
				grid[0][0] = 1;
    			grid[9][9] = 3;	
    			i=0,j=0;
    			riderMovement(grid);
    			rider(grid); // number of rider increasing 
				display(grid);					
			}	
		if(level==11){status = 1;} // game ends
		
					
	}while(status != 1 );	// game is not over
	
	cout << "\t....GAME  ENDS...."<<endl;
	
	return 0;
}

void display(int grid[10][10])
{
	cout<<endl<<"    -------------------------------"<<endl;
	for(int i=0 ; i<10 ; i++)
	{
		cout<<"    ";
		for(int j=0 ;j<10;j++)
		{
			
			if(j==0){cout<<"|";}
			if(grid[i][j]==0)	{cout << "  ";}			//	empty
			else if(grid[i][j]==1)	{ cout<<" F";}		// frodo			
			else if(grid[i][j]==2)	{ cout<<" R";}	    // rider
			else if(grid[i][j]==3)	{ cout<<" E";}		// exit
			cout << "|";
		}
		cout <<endl <<"    -------------------------------"<< endl ;
	}	
	cout << endl;
}

void rider(int grid[10][10])
{
	for(int k=0;k<3;k++)
	{
		int x =	rand()%9+1;
		int y = rand()%9+1;
		int z =  rand()%9+1;
		
		if(grid[x][y]==0){grid[x][y]=2;} // first random space for Rider
		// reserve space to avoid alluvion
		else if(grid[y][x]==0){grid[y][x]=2;} 
		else if(grid[x][z]==0){grid[x][z]=2;}
		else if(grid[z][x]==0){grid[z][x]=2;}
		else if(grid[y][z]==0){grid[y][z]=2;}
		else if(grid[z][y]==0){grid[z][y]=2;}
	}
}

void riderMovement(int grid[10][10])
{
	for(int i=0;i<10;i++)
	{
		for(int j=0;j<10;j++)
		{						
			if(grid[i][j]==2)			
			{			
				int x = rand()%9+1;
				switch(x)						
				{			
					case 1 :			
						if(i+1>9 || j-1 < 0){grid[i][j]=2;} // edgewise
						else if(grid[i+1][j-1]==0) // valid movement
						{
							grid[i+1][j-1]=2;
							grid[i][j]=0;
						}
						else{grid[i][j]=2;}	 // invalid movement							
						break;
					case 2 :
						if(i+1>9){grid[i][j]=2;}
						else if(grid[i+1][j]==0)
						{
							grid[i+1][j]=2;
							grid[i][j]=0;
						}	
						else{grid[i][j]=2;}							
						break;
					case 3 :
						if(i+1>9 || j+1>9){grid[i][j]=2;}
						else if(grid[i+1][j-1]==0)
						{
							grid[i+1][j+1]=2;
							grid[i][j]=0;
						}				
						else{grid[i][j]=2;}				
						break;
					case 4 :
						if( j-1 < 0){grid[i][j]=2;}
						else if(grid[i][j-1]==0)
						{
							grid[i][j-1]=2;
							grid[i][j]=0;
						}				
						else{grid[i][j]=2;}				
						break;
					case 6 :
						if( j+1 >9){grid[i][j]=2;}
						else if(grid[i][j+1]==0)
						{
							grid[i][j+1]=2;
							grid[i][j]=0;
						}				
						else{grid[i][j]=2;}				
						break;
					case 7 :
						if(i-1<0 || j-1 < 0){grid[i][j]=2;}
						else if(grid[i-1][j-1]==0)
						{
							grid[i-1][j-1]=2;
							grid[i][j]=0;
						}				
						else{grid[i][j]=2;}				
						break;
					case 8 :
						if(i-1 < 0 ){grid[i][j]=2;}
						else if(grid[i-1][j]==0)
						{
							grid[i-1][j]=2;
							grid[i][j]=0;
						}				
						else{grid[i][j]=2;}				
						break;
					case 9 :
						if(i-1<0 || j+1>9){grid[i][j]=2;}
						else if(grid[i-1][j+1]==0)
						{
							grid[i-1][j+1]=2;
							grid[i][j]=0;
						}				
						else{grid[i][j]=2;}				
						break;	
					default :
						grid[i][j]=2;
						break;											
				}
			}
		}
	}			
}
